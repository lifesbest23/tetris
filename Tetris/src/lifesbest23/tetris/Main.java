package lifesbest23.tetris;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class Main extends JFrame{
	private static final long serialVersionUID = 3206847208968227189L;

	public Main(){
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400, 600);
		this.setTitle("Tetris");
		this.setIconImage(new ImageIcon(getClass().getResource("/resources/images/logo.png")).getImage());
		
		Display d = new Display();
		this.add(d);
		
		this.addComponentListener(d);
		
		Dimension k = d.getPreferredSize();
		Insets i = this.getInsets();
		this.setMinimumSize(new Dimension(k.width + i.right + i.left, k.height + i.bottom + i.top));
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public static void main(String[] str){
		new Main();
	}

}

