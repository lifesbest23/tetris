package lifesbest23.tetris;

import java.util.Arrays;

import lifesbest23.tetris.TetrisPiece.Shape;

public class Game {
	
	public int curX, curY;
	public TetrisPiece curPiece, nextPiece;
	public final int height, width;
	public boolean isFallingFinished, isStarted, isPaused, isGameOver;
	
	public int numLinesRemoved = 0;
	public int numPiecesDropped = 0;
	public int secondsRunning = 0;
	
	public Shape[] board;
	
	int scores[] = new int[]{};
	private long startTime;
	
	public Game(int w, int h){
		height = h;
		width = w;
		
		curPiece = new TetrisPiece();
		nextPiece = new TetrisPiece();
		
		board = new Shape[w * h];
		clearBoard();
	}
	
	public void run(){
		if(!isStarted || isPaused || isGameOver)
			return;
		if(isFallingFinished){
			isFallingFinished = false;
			newPiece();
		}
		else{
			oneLineDown();
		}
		if((int) ((System.currentTimeMillis() - startTime) / 1000) >= 1){
			secondsRunning++;
			startTime = System.currentTimeMillis();
		}
	}
	//starts a new Game
	public void startNewGame(){
		isStarted = true;
		isPaused = false;
		isGameOver = false;
		isFallingFinished = false;
		numLinesRemoved = 0;
		numPiecesDropped = 0;
		
		clearBoard();

		nextPiece.setRandomShape();
		newPiece();
		
		newPiece();
		curPiece.setRandomShape();
		
		startTime = System.currentTimeMillis();
	}
	
	public void stop(){
		if(!isStarted)
			return;
		isStarted = false;
		clearBoard();
		this.curPiece.pieceShape = Shape.NoShape;
	}
	
	public void pause(){
		if(!isStarted || isGameOver)
			return;
		isPaused = !isPaused;
		startTime = System.currentTimeMillis();
	}
	
	public void abortGame(){
		isStarted = false;
	}
	
	public void oneLineDown(){
		if(!isStarted || isPaused || isGameOver)
			return;
		if(!tryMove(curPiece, curX, curY - 1))
			pieceDropped();
	}
	
	public void dropDown(){
		if(!isStarted || isPaused || isGameOver)
			return;
		int newY = curY;
		while(newY > 0){
			if(!tryMove(curPiece, curX, newY - 1))
				break;
			newY--;
		}
		pieceDropped();
	}
	
	public void rotateLeft(){
		if(!isStarted || isPaused || isGameOver)
			return;
		tryMove(curPiece.turnLeft(), curX, curY);
	}
	
	public void rotateRight(){
		if(!isStarted || isPaused || isGameOver)
			return;
		tryMove(curPiece.turnRight(), curX, curY);
	}
	
	public void moveLeft(){
		if(!isStarted || isPaused || isGameOver)
			return;
		tryMove(curPiece, curX - 1, curY);
	}
	
	public void moveRight(){
		if(!isStarted || isPaused || isGameOver)
			return;
		tryMove(curPiece, curX + 1, curY);
	}
	
	public Shape shapeAt(int x, int y){
		return board[y * width + x];
	}
	
	private void newPiece(){
		
		curPiece.setShape(nextPiece.getShape());
		nextPiece.setRandomShape();
//		curPiece.setRandomShape();

		curX = width/2 + 1;
		curY = height - 1 + curPiece.getMinY();
		
		if(!tryMove(curPiece, curX, curY)){
			isGameOver = true;
			scores = Arrays.copyOf(scores, scores.length + 1);
			scores[scores.length - 1] = this.numLinesRemoved*10 +
					this.numPiecesDropped + secondsRunning;
			secondsRunning = 0;
			Arrays.sort(scores);
		}
	}
	
	private void clearBoard(){
		for(int i = 0; i < board.length; i++)
			board[i] = Shape.NoShape;
	}
	
	private boolean tryMove(TetrisPiece newPiece, int newX, int newY){
		for(int i = 0; i < 4; i++){
			int x = newX + newPiece.x(i);
			int y = newY - newPiece.y(i);
			if( y < 0 || y >= height || x < 0 || x >= width)
				return false;
			if(shapeAt(x, y) != Shape.NoShape)
				return false;
		}
		
		curPiece = newPiece;
		curX = newX;
		curY = newY;
		return true;
	}
	
	private void pieceDropped(){
		for(int i = 0; i < 4; i++){
			int x = curX + curPiece.x(i);
			int y = curY - curPiece.y(i);
			board[y * width + x] = curPiece.pieceShape;
		}
		
		removeFullLines();
		
		numPiecesDropped++;
		
		if(!isFallingFinished)
			newPiece();
	}
	
	private void removeFullLines(){
		int numFullLines = 0;
		
		for(int i = height - 1; i >= 0; --i){
			boolean lineIsFull = true;
			
			for(int j =0; j < width; ++j){
				if(shapeAt(j, i) == Shape.NoShape){
					lineIsFull = false;
					break;
				}
				
			}
			
			if(lineIsFull){
				numFullLines++;
				for(int k = i; k < height - 1; ++k){
					for(int j = 0; j < width ; ++j){
						board[k * width + j] = shapeAt(j, k + 1);
					}
				}
			}
		}
		
		if(numFullLines > 0){
			numLinesRemoved += numFullLines;
			isFallingFinished = true;
			curPiece.setShape(Shape.NoShape);
		}
	}

}
