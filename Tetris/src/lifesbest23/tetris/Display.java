package lifesbest23.tetris;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import lifesbest23.tetris.TetrisPiece.Shape;
//add save HighScore and load HighScore then done
public class Display extends JPanel implements KeyListener, ActionListener, ComponentListener,
		MouseListener{
	private static final long serialVersionUID = 1827728358424057353L;
	
	Game game;
	int squareHeight = 25;
	int oldHeight, oldWidth;
	Rectangle2D pauseB, playB, stopB, helpB;
	Image pauseI, playI, stopI, replayI, helpI;
	Image helpScreen, title, highScore;
	boolean help = false;
	
	public Display(){
		super();
		
		game = new Game(10, 20);
		
		loadHighScore();
		initImage();
		
		this.addKeyListener(this);
		this.addMouseListener(this);

		this.setFocusable(true);
		this.setPreferredSize(new Dimension(game.width * squareHeight * 2 + 60, game.height* squareHeight + 40) );
		
		new Timer(500, this).start();
	}
	
	private void loadHighScore(){
		File f = new File(".highscores.lg23");
		if(f.exists()){
			try {
				Scanner sc = new Scanner(f);
				while(sc.hasNextInt()){
					game.scores = Arrays.copyOf(game.scores, game.scores.length + 1);
					game.scores[game.scores.length - 1] = sc.nextInt();
				}
				Arrays.sort(game.scores);
				sc.close();
			} catch (FileNotFoundException e) {e.printStackTrace();}
		}
		else
			game.scores = new int[]{300, 400, 500};
	}
	
	private void saveHighScore(){
		File f = new File(".highscores.lg23");
		try {
			FileWriter fw = new FileWriter(f);
			for(int i = 0; i < game.scores.length; i++){
				fw.write(game.scores[i] + " ");
			}
			fw.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	private void initImage(){
		ImageIcon i = new ImageIcon(getClass().getResource("/resources/images/play.png"));
		playI = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/pause.png"));
		pauseI = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/stop.png"));
		stopI = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/restart.png"));
		replayI = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/help.png"));
		helpI = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/helpScreen.png"));
		helpScreen = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/tetris.png"));
		title = i.getImage();
		i = new ImageIcon(getClass().getResource("/resources/images/highScore.png"));
		highScore = i.getImage();
	}
	
	private void help(){
		help = !help;
		if(!game.isPaused)
			game.pause();
	}
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		squareHeight = (int) ((getSize().getHeight() - 40) / game.height);
		
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
//		if(game.isStarted){
		
			for(int i = 0; i < game.height; ++i){
				for(int j = 0; j < game.width; ++j){
					Shape s= game.shapeAt(j, game.height - i - 1);
					
	//				if(s != Shape.NoShape)
						drawSquare(g, boardLeft + j*squareHeight, boardTop + i * squareHeight, s);
				}
			}
			if(game.curPiece.getShape() != Shape.NoShape){
				for(int i = 0; i < 4; i++){
					int x = game.curX + game.curPiece.x(i);
					int y = game.curY - game.curPiece.y(i);
					drawSquare(g, boardLeft + x*squareHeight, boardTop + (game.height - y - 1) * squareHeight,
							game.curPiece.getShape());
				}
			}
//		}
		
		drawFrame(g);
		
		if(game.isGameOver){
			//Draw Game Over String
			int x = boardLeft + (game.width * squareHeight)/2 - 60;
			g.setColor(Color.BLACK);
			g2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 17));
			g.fillRect(x, getHeight()/2 - 20, 120, 40);
			drawBorder((Graphics2D) g, true, x, getHeight()/2 - 20,
					x + 120, getHeight()/2 + 20);
			g.setColor(Color.WHITE);
			g.drawString("GAME OVER", x + 10, getHeight()/2 + 5);
		}
		if(game.isPaused){
			//Draw Pause String
			int x = boardLeft + (game.width * squareHeight)/2 - 45;
			g.setColor(Color.BLACK);
			g.fillRect(x, getHeight()/2 - 20, 90, 40);
			drawBorder((Graphics2D) g, true, x, getHeight()/2 - 20,
					x + 90, getHeight()/2 + 20);
			g.setColor(Color.WHITE);
			g2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 17));
			g2.drawString("PAUSE", x + 18, getHeight()/2 + 5);		
			//Draw Pause Button
			int di = (getWidth() - 8 - (boardLeft + gw + 20)) / 4;
			int y = boardTop;
			y += 5 * squareHeight + 45;
			y += getHeight() / 6 + 15;
			int i = boardLeft + gw + 20;
			drawBorder(g2, false, i, y, i + di - 10,	y + 30);
		}
		if(game.isStarted){
			int di = (getWidth() - 8 - (boardLeft + gw + 20)) / 4;
			int y = boardTop;
			y += 5 * squareHeight + 45;
			y += getHeight() / 6 + 15;
			int i = boardLeft + gw + 20 + di;
			drawBorder(g2, false, i, y, i + di - 10,	y + 30);			
		}
		drawNextPiece(g2);
		drawCurrentStats(g2);
		drawButtons(g2);
		drawHighscore(g2);
		drawTitle(g2);
		
		drawHelp(g2);
	}
	
	private void drawTitle(Graphics2D g){
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
		g.drawImage(title, boardLeft + gw + 20, boardTop, this);
	}
	
	private void drawHighscore(Graphics2D g){
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
		int y = boardTop;
		//next Shape Borders
		
		y += 30;
		y += 5 * squareHeight + 15;
		y += getHeight() / 6 + 15;
		y+= 30 + 15;
		int x = boardLeft + gw + 20 + 10;
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		g.drawString("Highscores", x + 30, y + g.getFont().getSize() + 5);
		y += 50;
		g.setFont(new Font(Font.SANS_SERIF, 0, 20));
		for(int i = 1; i < 6 && game.scores.length - i >= 0; i++){
			g.drawString(i + ".  -  " + game.scores[game.scores.length - i], x, y);
			y += 25;
		}
	}
	
	private void drawHelp(Graphics2D g){
		if(help)
			g.drawImage(helpScreen, 0, 0, this);
	}
	
	private void drawButtons(Graphics2D g){
		g.drawImage(pauseI, (int) pauseB.getX(), (int) pauseB.getY(), this);
		if(game.isStarted && !game.isGameOver)
			g.drawImage(stopI, (int) playB.getX(), (int) playB.getY(), this);
		else
			g.drawImage(playI, (int) playB.getX(), (int) playB.getY(), this);
		g.drawImage(replayI, (int) stopB.getX(), (int) stopB.getY(), this);
		g.drawImage(helpI, (int) helpB.getX(), (int) helpB.getY(), this);
		
	}
	
	private void drawCurrentStats(Graphics2D g){
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
		int y = boardTop;
		y += 5 * squareHeight + 45 + 20;
		int x = boardLeft + gw + 20 + 20;
		
		g.setFont(new Font(Font.SANS_SERIF, 0, 17));
		g.setColor(Color.BLACK);
		g.drawString(String.format("Lines Removed :     %d", game.numLinesRemoved), x, y);
		y += 20;
		g.drawString(String.format("Pieces Dropped:    %d", game.numPiecesDropped), x, y);
		y += 20;
		g.drawString(String.format("Time Elapsed    :    %d", game.secondsRunning), x, y);
		y += 20;
		g.drawString(String.format("        Score       :    %d", game.secondsRunning +
				game.numPiecesDropped + game.numLinesRemoved *10), x, y);
	}
	
	private void drawNextPiece(Graphics2D g){
		if(!game.isStarted)
			return;
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
		int x0 = boardLeft + gw + 20 + game.width/2 * squareHeight;
		int y0 = boardTop;
		y0 += 30 + 2*squareHeight;
		for(int i = 0; i < 4; i++){
			int x = x0 + game.nextPiece.x(i)* squareHeight;
			int y = y0 + game.nextPiece.y(i)* squareHeight;
			drawSquare(g, x, y, game.nextPiece.getShape());
		}
	}
	
	private void drawFrame(Graphics g){
		//Background
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		int gh = game.height*squareHeight;
		
		//Background
		g.setColor(new Color(0x969AD3));
		g.fillRect(0, 0, getWidth(), boardTop);
		g.fillRect(0, 0, boardLeft, getHeight());
		g.fillRect(boardLeft + gw, 0, getWidth(), getHeight());
		g.fillRect(0, boardTop + gh, getWidth(), getHeight());
		
		Graphics2D g2 = (Graphics2D) g;
		
		//outerBorder
		drawBorder(g2, true, 0, 0, getWidth(), getHeight());
		//Game Border
		drawBorder(g2, false, boardLeft, boardTop, boardLeft + gw, boardTop + gh);
		
		int y = boardTop;
		//next Shape Borders
		drawBorder(g2, false,
				boardLeft + gw + 20, y,
				getWidth() - boardLeft,	y + 25);
		y += 30;
		drawBorder(g2, false,
				boardLeft + gw + 20, y,
				getWidth() - boardLeft,	y + 5 * squareHeight);
		y += 5 * squareHeight + 15;
		//Current Stats
		drawBorder(g2, false,
				boardLeft + gw + 20, y,
				getWidth() - boardLeft, y + getHeight()/6);
		y += getHeight() / 6 + 15;
		//Pause / Replay / Stop & Play / Help
		int di = (getWidth() - 8 - (boardLeft + gw + 20)) / 4;
		for(int i = boardLeft + gw + 20; i < getWidth() - 20; i += di){
			drawBorder(g2, true, i, y, i + di - 10,	y + 30);
		}
		y+= 30 + 15;
		//HighScore
		drawBorder(g2, false,
				boardLeft + gw + 20, y,
				getWidth() - boardLeft, getHeight() - boardTop);
				
	}
	
	private void drawBorder(Graphics2D g, boolean out, int x, int y, int x2, int y2){
		if(out){
			g.setStroke(new BasicStroke(1));
			//drawBorders
			g.setColor(new Color(160, 160, 160));
			//outer Border
			g.drawLine(x, y, x2, y);
			g.drawLine(x, y, x, y2);
			
			g.setColor(new Color(200, 200, 200));
			//outer Border
			g.drawLine(x + 1, y + 1, x2 - 1, y + 1);
			g.drawLine(x + 1, y + 1, x + 1, y2 - 1);
			
			g.setColor(new Color(60, 60, 60));
			//outer Border
			g.drawLine(x2, y2, x2, y);
			g.drawLine(x2, y2, x, y2);
	
			g.setColor(new Color(30, 30, 30));
			//outer Border
			g.drawLine(x2 - 1, y2 - 1, x2 - 1, y + 1);
			g.drawLine(x2 - 1, y2 - 1, x + 1, y2 - 1);
		}
		else{
			g.setStroke(new BasicStroke(1));
			//drawBorders
			g.setColor(new Color(60, 60, 60));
			//outer Border
			g.drawLine(x, y, x2, y);
			g.drawLine(x, y, x, y2);
			
			g.setColor(new Color(30, 30, 30));
			//outer Border
			g.drawLine(x + 1, y + 1, x2 - 1, y + 1);
			g.drawLine(x + 1, y + 1, x + 1, y2 - 1);
			
			g.setColor(new Color(200, 200, 200));
			//outer Border
			g.drawLine(x2, y2, x2, y);
			g.drawLine(x2, y2, x, y2);
	
			g.setColor(new Color(160, 160, 160));
			//outer Border
			g.drawLine(x2 - 1, y2 - 1, x2 - 1, y + 1);
			g.drawLine(x2 - 1, y2 - 1, x + 1, y2 - 1);
		}
	}
	
	private void drawSquare(Graphics g, int x, int y, Shape s){
		Color colors[] = { new Color(120, 120, 120), new Color(204, 102, 102), 
				new Color(102, 204, 102), new Color(102, 102, 204), 
				new Color(204, 204, 102), new Color(204, 102, 204), 
				new Color(102, 204, 204), new Color(218, 170, 0)
		};
		Color c = colors[s.ordinal()];
		
		g.setColor(c);
		g.fillRect(x,  y, squareHeight, squareHeight);
		
		//make a surrounding
		g.setColor(c.brighter());
		g.drawLine(x, y, x + squareHeight, y);

		g.drawLine(x, y, x, y + squareHeight);
		

		g.setColor(c.darker());
		g.drawLine(x + squareHeight - 1, y + squareHeight - 1, x + squareHeight - 1, y);
		g.drawLine(x + squareHeight - 1, y + squareHeight - 1, x, y + squareHeight - 1);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(help)
			return;
		
		switch(e.getKeyCode()){
		case KeyEvent.VK_UP:
			game.rotateLeft();
			break;
		case KeyEvent.VK_LEFT:
			game.moveLeft();
			break;
		case KeyEvent.VK_RIGHT:
			game.moveRight();
			break;
		case KeyEvent.VK_DOWN:
			game.oneLineDown();
			break;
		case KeyEvent.VK_SPACE:
			game.dropDown();
			break;
		case KeyEvent.VK_P:
			game.pause();
			break;
		case KeyEvent.VK_N:
			game.startNewGame();
			break;
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0) { }

	@Override
	public void keyTyped(KeyEvent arg0) { }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		game.run();
		if(game.isGameOver)
			saveHighScore();
		repaint();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent e) {
		JFrame f = (JFrame) e.getComponent();
		
		if(oldHeight == f.getContentPane().getBounds().getHeight() &&
				oldWidth == f.getContentPane().getBounds().getWidth())
			return;
		
		if(oldHeight != f.getContentPane().getBounds().getHeight()){
			squareHeight = (int) ((f.getContentPane().getBounds().getHeight() - 40) / game.height);
		}
		else if(oldWidth != f.getContentPane().getBounds().getHeight()){
			squareHeight = (int) (((f.getContentPane().getBounds().getWidth() - 60)/2) / game.width);
		}
		
		oldWidth = (int) f.getContentPane().getBounds().getWidth();
		oldHeight = (int) f.getContentPane().getBounds().getHeight();

		f.setSize(new Dimension(game.width * squareHeight * 2 + 60 + 
					f.getInsets().left + f.getInsets().right,
				game.height* squareHeight + 40 + 
					f.getInsets().top + f.getInsets().bottom));
		
		int boardTop = (int) ( getSize().getHeight() - game.height * squareHeight )/2;
		int boardLeft = 20;
		int gw = game.width*squareHeight;
		
		//Pause / Replay / Stop & Play / Help
		int di = (getWidth() - 8 - (boardLeft + gw + 20)) / 4;
		int y = boardTop;
		y += 5 * squareHeight + 45;
		y += getHeight() / 6 + 15;
		int i = boardLeft + gw + 20;
		
		pauseB = new Rectangle(i, y, di, 20);
		i += di;
		playB = new Rectangle(i, y, di, 20);
		i += di;
		stopB = new Rectangle(i, y, di, 20);
		i += di;
		helpB = new Rectangle(i, y, di, 20);
		i += di;
		
		title = title.getScaledInstance( getWidth() - boardLeft - (boardLeft + gw + 20), 25,
				Image.SCALE_FAST);
	}

	@Override public void componentShown(ComponentEvent arg0) {}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if(help){
			help();
			repaint();
			return;
		}
		Point2D p = e.getPoint();
		if(pauseB.contains(p))
			game.pause();
		if(playB.contains(p)){
			if(game.isStarted && !game.isGameOver)
				game.stop();
			else
				game.startNewGame();
		}
		else if(stopB.contains(p))
			game.startNewGame();
		else if(helpB.contains(p))
			help();
			
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
