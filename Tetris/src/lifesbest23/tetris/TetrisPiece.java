package lifesbest23.tetris;

import java.util.Random;

public class TetrisPiece {
	public enum Shape{
		NoShape, SquareShape, SShape, ZShape, TShape, LineShape, LShape, MirrordLShape
	}
	
	public int x, y;
	public Shape pieceShape;
	private int[][] coords;
	
	public TetrisPiece(){
		coords = new int[4][2];
		setShape(Shape.NoShape);
	}
	
	public void setShape(Shape s){
		//relative coordinates for all the shapes
		int[][][] coordsTable = new int[][][]{
				{ {0,0}, 	{0,0}, 	{0,0}, 	{0,0} },	//NoShape
				{ {0,0}, 	{0,1}, 	{1,0}, 	{1,1} },	//SquareShape
				{ {0,-1},	{0,0}, 	{1,0}, 	{1,1} },	//SShape
				{ {1,-1}, 	{1,0}, 	{0,0}, 	{0,1} },	//ZShape
				{ {-1,0}, 	{0,0}, 	{1,0},	{0,1} },	//TShape
				{ {0,-1}, 	{0,0}, 	{0,1},	{0,2} },	//LineSHape
				{ {-1,-1}, 	{0,-1}, {0,0}, 	{0,1} },	//LShape
				{ {1,-1}, 	{0,-1}, {0,0}, 	{0,1} }		//MirrordLShape
		};
		//get the coordinates for the chosen Shape
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 2; j++){
				coords[i][j] = coordsTable[s.ordinal()][i][j];
			}
		}
		
		pieceShape = s;
	}
	
	//sets a random shape (excluding noshape)
	public void setRandomShape(){
		Random r = new Random();
		int x = Math.abs(r.nextInt()) % 7 + 1;
		Shape[] shapes = Shape.values();
		setShape(shapes[x]);
	}
	
	private void setX(int index, int x){
		coords[index][0] = x;
	}
	
	private void setY(int index, int y){
		coords[index][1] = y;
	}
	
	public int x(int index){
		return coords[index][0];
	}
	
	public int y(int index){
		return coords[index][1];
	}
	
	public Shape getShape(){
		return Shape.values()[this.pieceShape.ordinal()];
	}
	
	public int getMinX(){
		int m = 0;
		for(int i = 0; i < 4; i++)
			m = Math.min(m, coords[i][0]);
		return m;
	}
	
	public int getMinY(){
		int m = 0;
		for(int i = 0; i < 4; i++)
			m = Math.min(m, coords[i][1]);
		return m;
	}
	
	public TetrisPiece turnLeft(){
		//if the Shape is square it is not rotated
		if(pieceShape == Shape.SquareShape)
			return this;
		
		TetrisPiece result = new TetrisPiece();
		result.pieceShape = pieceShape;
		
		for(int i = 0; i < 4; ++i){
			result.setX(i, y(i));
			result.setY(i, -x(i));
		}
		
		return result;
	}
	
	public TetrisPiece turnRight(){
		//if the Shape is square it is not rotated
		if(pieceShape == Shape.SquareShape)
			return this;
		
		TetrisPiece result = new TetrisPiece();
		result.pieceShape = pieceShape;
		
		for(int i = 0; i < 4; ++i){
			result.setX(i, -y(i));
			result.setY(i, x(i));
		}
		
		return result;
	}
}
